const loadStates = [
    'On the way for Pick Up',
    'Arrived to Pick Up',
    'On the way for Delivery',
    'Delivery arrived'
];

const truckStates = {
    inServise: 'InServise',
    onLoad: 'OnLoad'
}

module.exports = {
    loadStates,
    truckStates
}