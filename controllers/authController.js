const User = require('../models/user');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const statusCode = require('http-status');

async function registerUser(req, res) {
    const {name, role, email, password} = req.body; 

    const userExists = await User.findOne({email: email});
    if (userExists) {
        return res.status(statusCode.BAD_REQUEST).send('User already exists')
    }

    const hashedPass = await bcrypt.hash(password, 10);
    let user;

    if (role === 'DRIVER') {
        user = new User({
            name: name,
            role: role,
            email: email,
            password: hashedPass,
            assigned_load: false
        })
    } else {
        user = new User({
            name: name,
            role: role,
            email: email,
            password: hashedPass
        })
    }

    try {
        const newUser = await user.save();
        res.status(statusCode.OK).json({"message": "Profile has been created successfully"})
    } catch (err) {
        res.status(statusCode.BAD_REQUEST).json({"message": err.message});
    }
}


async function loginUser(req, res) {
    const foundUser = await User.findOne({email: req.body.email});
    if (!foundUser) {
        return res.status(statusCode.BAD_REQUEST).json({"message":'User doesn`t exist'})
    }

    const match = await bcrypt.compare(req.body.password, foundUser.password);
    if (!match) {
        return res.status(statusCode.FORBIDDEN).send('Access denied')
    }

    const token = jwt.sign({
        _id: foundUser._id,
        role: foundUser.role
        },
        process.env.ACCESS_TOKEN
    )

    try {
        const user = await User.findByIdAndUpdate(foundUser._id, {token: token});
        if (!user) {
            return res.status(statusCode.BAD_REQUEST).json({"message": "not found"});
        }
        res.json({jwt_token: token})
    } catch (error) {
        res.status(statusCode.INTERNAL_SERVER_ERROR).json({"message": error.message});
    }
}

async function logoutUser(req, res) {
    const newToken = jwt.sign({
        _id: req.user._id,
        },
        process.env.LOGOUT_TOKEN,
    )

    try {
        const updatedUser = await User.findByIdAndUpdate(req.user._id, {token: newToken});
        if (!updatedUser) {
            return res.status(statusCode.BAD_REQUEST).json({"message": "User not found"});
        }
        res.status(statusCode.OK).json({"message": "User logged out"});
    } catch (err) {
        res.status(statusCode.INTERNAL_SERVER_ERROR).json({"message": err.message});
    }   
}

async function forgotPassword(req, res) {
    const { email } = req.body;

    const foundUser = await User.findOne({email: email});
    if (!foundUser) {
        return res.status(statusCode.BAD_REQUEST).json({"message": "User not registered"})
    }

    const secret = process.env.ACCESS_TOKEN + foundUser.password;

    const token = jwt.sign({
        _id: foundUser._id,
        role: foundUser.role
    },
        secret,
    {expiresIn: '5m'}
    )
    const link = `http://localhost:8080/api/auth/reset_password/${foundUser._id}/${token}`;

}

module.exports = {
    registerUser,
    loginUser,
    forgotPassword,
    logoutUser
}