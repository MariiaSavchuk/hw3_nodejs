const Load = require('../models/load');
const User = require('../models/user');
const Truck = require('../models/truck');
const statusCode = require('http-status');
const validateLoad = require('../config/loadValidation');
const { loadStates, truckStates } = require('../config/constants');

async function createLoad(req, res) {
    const {
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions
    } = req.body;

    const userId = req.user._id;

    const userRole = req.user.role;
    if (userRole !== 'SHIPPER') {
        return res.status(statusCode.BAD_REQUEST).json({"message": "Transaction is not allowed for current user"})
    }
    const load = new Load({
        created_by: userId,
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions,
        logs: [
            {
                "message": "Load has been created"
            }
        ]
    })

    try {
        const newLoad = await load.save();
        res.status(statusCode.OK).json({"message": "Load has been created successfully"})
    } catch (err) {
        res.status(statusCode.INTERNAL_SERVER_ERROR).json({"message": err.message});
    }
}

async function getLoads(req, res) {
    try {
        const userRole = req.user.role;
        if (userRole !== 'SHIPPER') {
            return res.status(statusCode.BAD_REQUEST).json({"message": "Transaction is not allowed for current user"})
        }

        const userId = req.user._id;
        let { limit, offset, status } = req.body;

        const loads = status ? await Load.find({created_by: userId, status: status}) : await Load.find({created_by: userId});

        if (!limit || limit > 50) {
            limit = 10;
        }

        if (!offset || offset > loads.length) {
            offset = 0;
        }

        if (offset > 0 && offset < loads.length) {
            loads.splice(0, offset)
        }

        if (limit > 0 && limit < loads.length) {
            loads.splice(limit)
        }

        res.status(statusCode.OK).json({"loads": loads})
    } catch (err) {
        res.status(statusCode.INTERNAL_SERVER_ERROR).json({"message": err.message});
    }
}

async function getActiveLoad(req, res) {
    const userId = req.user._id;

    const userRole = req.user.role;
    if (userRole !== 'DRIVER') {
        return res.status(statusCode.BAD_REQUEST).json({"message": "Transaction is not allowed for current user"})
    }

    try {
        const foundLoad = await Load.findOne({assigned_to: userId});
        res.status(statusCode.OK).json({"load": foundLoad})
    } catch (err) {
        res.status(statusCode.INTERNAL_SERVER_ERROR).json({"message": err.message});
    }
}

async function updateNewLoads(req, res) {
    const {
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions
    } = req.body;

    const loadId = req.params.id;
    const userId = req.user._id;
    const userRole = req.user.role;

    if (userRole !== 'SHIPPER') {
        return res.status(statusCode.BAD_REQUEST).json({"message": "Transaction is not allowed for current user"})
    }

    if (!loadId) {
        return res.status(statusCode.BAD_REQUEST).json({"message": "No id specified"})
    }

    try {
        const load = await Load.findOne({_id: loadId, created_by: userId, status: 'NEW'});

        if (!load) {
            return res.status(statusCode.BAD_REQUEST).json({"message": "Cannot find load"});
        }

        const updatedLoad = await Load.findByIdAndUpdate(loadId, {
            name,
            payload,
            pickup_address,
            delivery_address,
            dimensions
        })
        res.status(statusCode.OK).json({"message": "Load details has been changed successfully"});
    } catch (err) {
        res.status(statusCode.INTERNAL_SERVER_ERROR).json({"message": err.message});
    }
}

async function deleteNewLoads(req, res) {
    const loadId = req.params.id;
    const userId = req.user._id;
    const userRole = req.user.role;

    if (userRole !== 'SHIPPER') {
        return res.status(statusCode.BAD_REQUEST).json({"message": "Transaction is not allowed for current user"})
    }

    if (!loadId) {
        return res.status(statusCode.BAD_REQUEST).json({"message": "No id specified"})
    }

    try {
        const load = await Load.findOne({_id: loadId, created_by: userId});
        if (load.status !== 'NEW') {
            return res.status(statusCode.BAD_REQUEST).json({"message": "You can only delete new loads"})
        }
        const deleteLoad = await Load.findOneAndDelete({_id: loadId, created_by: userId});
        res.status(statusCode.OK).json({"message": "Load has been deleted successfully"});
    } catch (err) {
        res.status(statusCode.INTERNAL_SERVER_ERROR).json({"message": err.message});
    }
}

async function postLoad(req, res) {
    const loadId = req.params.id;
    const userRole = req.user.role;

    if (userRole !== 'SHIPPER') {
        return res.status(statusCode.BAD_REQUEST).json({"message": "Transaction is not allowed for current user"})
    }

    if (!loadId) {
        return res.status(statusCode.BAD_REQUEST).json({"message": "No id specified"})
    }

    try {
        const load = await Load.findOne({_id: loadId});
        if (!load) {
            return res.status(statusCode.BAD_REQUEST).json({"message": "No load with such id found"})
        }
        if (load.assigned_to) {
            return res.json({"message": "Load has been assigned"})
        }

        const logs = load.logs;

        const loadPosted = await Load.findByIdAndUpdate(loadId, {
            status: 'POSTED',
            logs: [
                ...logs,
                {
                    message: "Load status changed to POSTED"
                }
            ]
        });

        const trucks = await Truck.find({assigned_to: {$exists: true}});
        const {width, length, height} = load.dimensions;
        const payload = load.payload;
        let truckFound;

        for (let i = 0; i < trucks.length; i++) {
            const loadFits = validateLoad(trucks[i].type, width, length, height, payload);
            if (loadFits && trucks[i].status === truckStates.inServise) {
                truckFound = trucks[i];
                break;
            }
        }

        if (!truckFound) {
            const loadNotAssigned = await Load.findByIdAndUpdate(loadId, {
                status: 'NEW',
                logs: [
                    ...logs,
                    {
                        message: "Load was not assigned"
                    }
                ]
            })
            return res.json({
                "message": "Load wasn't assigned successfully",
                "driver_found": false
            })
        }

        const updatedTruck = await Truck.findByIdAndUpdate(truckFound._id, {status: truckStates.onLoad});

        const loadAssigned = await Load.findByIdAndUpdate(loadId, {
            assigned_to: truckFound.assigned_to, 
            status: 'ASSIGNED', 
            state: loadStates[0],
            logs: [
                ...logs,
                {
                    message: `Load assigned to driver with id ${truckFound.assigned_to}`,
                    time: Date.now()
                }
            ]
        });

        res.status(statusCode.OK).json({
            "message": "Load has been posted successfully",
            "driver_found": true,
        })
    } catch (err) {
        res.status(statusCode.INTERNAL_SERVER_ERROR).json({"message": err.message});
    }
}

async function changeLoadState(req, res) {
    const userRole = req.user.role;
    const userId = req.user._id;
    if (userRole !== 'DRIVER') {
        return res.status(statusCode.BAD_REQUEST).json({"message": "Transaction is not allowed for current user"})
    }
    try {
        const activeLoad = await Load.findOne({assigned_to: userId});

        if (!activeLoad) {
            return res.json({"message": "No active load found"})
        }
        const loadState = activeLoad.state;
        const loadLogs = activeLoad.logs;

        let newLoadState = '';
        let newLogs = [];

        const index = loadStates.findIndex(state => state === loadState);
        if (index >= 0 && index < loadStates.length) {
            newLoadState = loadStates[index + 1];

            if (newLoadState === loadStates[3]) {
                newLogs = [...loadLogs, {"message": `Load state updated to ${newLoadState}`}]
                const shippedLoad = await Load.findByIdAndUpdate(activeLoad._id, {state: newLoadState, status: 'SHIPPED', logs: newLogs, assigned_to: null});
                const activeTruck = await Truck.findOne({assigned_to: userId});
    
                if (activeTruck) {
                    const unloadedTruck = await Truck.findByIdAndUpdate(activeTruck._id, {status: truckStates.inServise});
                }
                return res.status(statusCode.OK).json({"message": `Load state changed to ${newLoadState}`});
            }

            newLogs = [...loadLogs, {"message": `Load state updated to ${newLoadState}`}]
            const updatedLoad = await Load.findByIdAndUpdate(activeLoad._id, {state: newLoadState, logs: newLogs});
            res.status(statusCode.OK).json({"message": `Load state changed to ${newLoadState}`});
        }

    } catch (err) {
        res.status(statusCode.INTERNAL_SERVER_ERROR).json({"message": err.message});
    }
}

async function getShippingInfo(req, res) {
    const loadId = req.params.id;

    if (!loadId) {
        return res.status(statusCode.BAD_REQUEST).json({"message": "No id specified"})
    }

    const userRole = req.user.role;

    if (userRole !== 'SHIPPER') {
        return res.status(statusCode.BAD_REQUEST).json({"message": "Transaction is not allowed for current user"})
    }

    try {
        const foundLoad = await Load.findOne({_id: loadId});
        const driverId = foundLoad.assigned_to;
        const foundTruck = await Truck.findOne({assigned_to: driverId});
        
        res.status(statusCode.OK).json({
            "load": foundLoad,
            "truck": foundTruck
        })
    } catch (err) {
        res.status(statusCode.INTERNAL_SERVER_ERROR).json({"message": err.message});
    }
}

module.exports = {
    createLoad,
    getLoads,
    updateNewLoads,
    deleteNewLoads,
    postLoad,
    getActiveLoad,
    changeLoadState,
    getShippingInfo
}