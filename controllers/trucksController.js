const Truck = require('../models/truck');
const statusCode = require('http-status');
const {truckStates} = require('../config/constants');

async function createTruck(req, res) {
    const {type, status} = req.body;
    const userId = req.user._id;

    const userRole = req.user.role;
    if (userRole !== 'DRIVER') {
        return res.status(statusCode.BAD_REQUEST).json({"message": "Transaction is not allowed for current user"})
    }

    const truck = new Truck({
        created_by: userId,
        type: type,
        status: status
    })
    try {
        const newTruck = await truck.save();
        res.status(statusCode.OK).json({"message": "Truck has been created successfully"})
    } catch (err) {
        res.status(statusCode.INTERNAL_SERVER_ERROR).json({"message": err.message});
    }
}

async function getTrucks(req, res) {
    try {
        const userRole = req.user.role;
        if (userRole !== 'DRIVER') {
            return res.status(statusCode.BAD_REQUEST).json({"message": "Transaction is not allowed for current user"})
        }
        const userId = req.user._id;
        const trucks = await Truck.find({created_by: userId});

        res.status(statusCode.OK).json({"trucks": trucks})
    } catch (err) {
        res.status(statusCode.INTERNAL_SERVER_ERROR).json({"message": err.message});
    }
}

async function getTruckById(req, res) {
    try {
        const truckId = req.params.id;
        const userId = req.user._id;
        const userRole = req.user.role;

        if (userRole !== 'DRIVER') {
            return res.status(statusCode.BAD_REQUEST).json({"message": "Transaction is not allowed for current user"})
        }

        if (!truckId) {
            return res.status(statusCode.BAD_REQUEST).json({"message": "No id specified"})
        }

        const truck = await Truck.findOne({created_by: userId, _id: truckId});
        res.status(statusCode.OK).json({"truck": truck});
    } catch (err) {
        res.status(statusCode.INTERNAL_SERVER_ERROR).json({"message": err.message});
    }
}

async function updateTruckById(req, res) {
    try {
        const userRole = req.user.role;

        if (userRole !== 'DRIVER') {
            return res.status(statusCode.BAD_REQUEST).json({"message": "Transaction is not allowed for current user"})
        }

        const { type } = req.body;
        const truckId = req.params.id;
        const userId = req.user._id;

        if (!truckId) {
            return res.status(statusCode.BAD_REQUEST).json({"message": "No id specified"})
        }

        const activeTruck = await Truck.findOne({assigned_to: userId, status: truckStates.onLoad});
        if (activeTruck) {
            return res.status(statusCode.BAD_REQUEST).json({"message": "You can't change any truck info while you are on load"})
        }

        const truck = await Truck.findOne({_id: truckId});
        if (!truck) {
            return res.status(statusCode.BAD_REQUEST).json({"message": "No truck with such id found"})
        } 
        
        if (truck.assigned_to?.toString() === userId) {
            return res.status(statusCode.BAD_REQUEST).json({"message": "Cannot update assigned truck info"})
        }

        const updatedTruck = await Truck.findByIdAndUpdate(truckId, {type: type});
        res.status(statusCode.OK).json({"message": "Truck details has been changed successfully"});
    } catch (err) {
        res.status(statusCode.INTERNAL_SERVER_ERROR).json({"message": err.message});
    }
}

async function deleteTruckById(req, res) {
    try {
        const truckId = req.params.id;
        const userId = req.user._id;
        const userRole = req.user.role;

        if (userRole !== 'DRIVER') {
            return res.status(statusCode.BAD_REQUEST).json({"message": "Transaction is not allowed for current user"})
        }

        if (!truckId) {
            return res.status(statusCode.BAD_REQUEST).json({"message": "No id specified"})
        }

        const truck = await Truck.findOne({_id: truckId});
        if (!truck) {
            return res.status(statusCode.BAD_REQUEST).json({"message": "No truck with such id found"})
        }

        if (truck.assigned_to?.toString() === userId) {
            return res.status(statusCode.BAD_REQUEST).json({"message": "Cannot delete assigned truck"})
        }

        const deleteTruck = await Truck.findByIdAndDelete({created_by: userId, _id: truckId});
        res.status(statusCode.OK).json({"message": "Truck has been deleted successfully"});
    } catch (err) {
        res.status(statusCode.INTERNAL_SERVER_ERROR).json({"message": err.message});
    }
}

async function assignTruck(req, res) {
    const truckId = req.params.id;
    const userId = req.user._id;

    if (!truckId) {
        return res.status(statusCode.BAD_REQUEST).json({"message": "No id specified"})
    }

    const userRole = req.user.role;
    if (userRole !== 'DRIVER') {
        return res.status(statusCode.BAD_REQUEST).json({"message": "Transaction is not allowed for current user"})
    }

    try {
        const truck = await Truck.findOne({_id: truckId});
        if (!truck) {
            return res.status(statusCode.BAD_REQUEST).json({"message": "No truck with such id found"})
        }
        const assignedTruck = await Truck.findOne({assigned_to: userId});
        if (assignedTruck) {
            return res.status(statusCode.BAD_REQUEST).json({"message": "You already have assigned truck"})
        }

        const updatedTruck = await Truck.findByIdAndUpdate(truckId, {assigned_to: userId});
        res.status(statusCode.OK).json({"message": "Truck has been assigned successfully"})
    } catch (err) {
        res.status(statusCode.INTERNAL_SERVER_ERROR).json({"message": err.message});
    }
}    

module.exports = {
    createTruck,
    getTrucks,
    getTruckById,
    updateTruckById,
    deleteTruckById,
    assignTruck,
}