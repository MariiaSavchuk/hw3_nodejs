const User = require('../models/user');
const Truck = require('../models/truck');
const bcrypt = require('bcryptjs');
const statusCode = require('http-status');
const {truckStates} = require('../config/constants');

async function getUser(req, res) {
    try {
        const user = await User.findById({ _id: req.user._id });
        if (!user) {
            return res.status(statusCode.BAD_REQUEST).json({"message": "User not found"});
        }
        const {_id, name, role, email, createdDate, assigned_load } = user;
        res.status(statusCode.OK).json({"user": {_id, name, role, email, createdDate, assigned_load}});
    } catch (err) {
        res.status(statusCode.INTERNAL_SERVER_ERROR).json({"message": err.message});
    }
}

async function updateUser(req, res) {
    try {
        const userRole = req.user.role;
        if (userRole === 'DRIVER') {
            const activeTruck = await Truck.findOne({assigned_to: req.user._id, status: truckStates.onLoad});
            if (activeTruck) {
                return res.status(statusCode.BAD_REQUEST).json({"message": "You can't change profile info while you on load"})
            }
        }

        const {oldPassword, newPassword} = req.body;
        const user = await User.findById({ _id: req.user._id });
        const match = await bcrypt.compare(oldPassword, user.password);
        const hashedPass = await bcrypt.hash(newPassword, 10);

        const updatedUser = await User.findByIdAndUpdate(req.user._id, {password: hashedPass});
        if (!updatedUser) {
            return res.status(statusCode.BAD_REQUEST).json({"message": "not found"});
        }
        res.status(statusCode.OK).json({"message": "Password changed successfully"});
    } catch (error) {
        res.status(statusCode.INTERNAL_SERVER_ERROR).json({"message": error.message});
    }
}

async function deleteUser(req, res) {
    try {
        const deletedUser = await User.findByIdAndDelete({ _id: req.user._id });
        if (!deletedUser) {
            return res.status(statusCode.BAD_REQUEST).json({"message": "User not found"});
        }
        res.status(statusCode.OK).json({"message": "Profile has been deleted successfully"});
    } catch (err) {
        res.status(statusCode.INTERNAL_SERVER_ERROR).json({"message": err.message});
    }
}

module.exports = {
    getUser,
    updateUser,
    deleteUser
}