const jwt = require('jsonwebtoken');
const jwt_decode = require("jwt-decode");
const statusCode = require('http-status');
const User = require('../models/user');

async function verifyJWT(req, res, next) {

    const token = req.headers["authorization"].split(' ')[1];
    const payload = jwt_decode(token);

    if (!token) {
        return res.status(statusCode.UNAUTHORIZED).send('Access denied!');
    }

    try {
        const verified = jwt.verify(token, process.env.ACCESS_TOKEN);
        const foundUser = await User.findById({ _id: verified._id });
        if (!foundUser) {
            return res.status(statusCode.BAD_REQUEST).json({"message": "User not found"});
        } else {
            if (foundUser.token === token) {
                req.user = verified;
                next()
            } else {
                return res.status(statusCode.BAD_REQUEST).json({"message": "User logged out"});
            }
        }
    } catch (error) {
        res.status(statusCode.BAD_REQUEST).send('Invalid token');
    }
}

module.exports = verifyJWT;