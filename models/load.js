const mongoose = require('mongoose')

const dimensionsSchema = new mongoose.Schema({
    width: {
        type: Number
    },
    length: {
        type: Number
    },
    height: {
        type: Number
    }
})
const logsSchema = new mongoose.Schema({
    message: {
        type: String
    },
    time: {
        type: Date,
        default: Date.now
    }
})

const loadSchema = new mongoose.Schema({
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    assigned_to: {
        type: mongoose.Schema.Types.ObjectId,
    },
    status: {
        type: String,
        default: 'NEW'
    },
    state: {
        type: String,
        default: ''
    },
    name: {
        type: String,
        required: true
    },
    payload: {
        type: Number,
        required: true
    },
    pickup_address: {
        type: String,
        required: true,
        max: 255
    },
    delivery_address: {
        type: String,
        required: true,
        max: 255
    },
    dimensions: {
        type: dimensionsSchema,
        required: true
    },
    logs: [{ 
        type : logsSchema, 
    }],
    created_date: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('Load', loadSchema);