const express = require('express');
const router = express.Router();
const verifyJWT = require('../middlewares/verifyJWT');

const {registerUser, loginUser, forgotPassword, logoutUser} = require('../controllers/authController');

router.post('/register', registerUser);
router.post('/login', loginUser)
router.post('/logout', verifyJWT, logoutUser)
router.post('/forgot_password', forgotPassword)

module.exports = router;