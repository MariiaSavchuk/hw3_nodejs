const express = require('express');
const router = express.Router();
const verifyJWT = require('../middlewares/verifyJWT');

const {getUser, deleteUser, updateUser} = require('../controllers/usersController')

router.get('/me', verifyJWT, getUser);
router.delete('/me', verifyJWT, deleteUser)
router.patch('/me/password', verifyJWT, updateUser)

module.exports = router;